function onload(){
    var mymap = L.map('mapid', { minZoom: 0, maxZoom: 18 }).setView([59.925, 10.705], 12);
    var layer = new L.tileLayer(
        'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', 
        {attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'}
    ).addTo(mymap);
    loop(mymap,layer);
}
function loop(mymap,layer){
        mymap.addLayer(layer);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
             if (this.readyState == 4 && this.status == 200) {
                var myArr = JSON.parse(this.responseText);
                vehicles = myArr.Siri.ServiceDelivery.VehicleMonitoringDelivery[0].VehicleActivity;
                var markerClusters = L.markerClusterGroup();
                //console.log(vehicles);
                for (vehicle in vehicles){
                     if (vehicles[vehicle].hasOwnProperty("MonitoredVehicleJourney")){
                        if (vehicles[vehicle].MonitoredVehicleJourney.hasOwnProperty("VehicleLocation")){
                            data = vehicles[vehicle].MonitoredVehicleJourney.VehicleLocation;
                            console.log(data);
                            var m = L.marker([data.Latitude, data.Longitude]);
                            markerClusters.addLayer( m );
                        }
                     }
                }
                mymap.addLayer( markerClusters );
             }
        };
        xhttp.open("GET", "https://api.entur.io/realtime/v1/rest/vm", true);
        xhttp.setRequestHeader("Content-type", "application/json");
        xhttp.send();
}
